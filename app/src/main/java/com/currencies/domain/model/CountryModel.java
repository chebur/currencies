package com.currencies.domain.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CountryModel {

    @SerializedName("flag")
    private String flag;

    @SerializedName("currencies")
    private List<CurrencyModel> currencies;

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public List<CurrencyModel> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<CurrencyModel> currencies) {
        this.currencies = currencies;
    }
}
