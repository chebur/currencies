package com.currencies.domain.repository;

import com.currencies.CurrenciesApplication;
import com.currencies.domain.api.ApiHolder;
import com.currencies.domain.api.CountriesApi;
import com.currencies.domain.model.CountryModel;
import com.currencies.domain.model.CurrencyModel;
import com.currencies.domain.model.RatesModel;
import com.currencies.persistence.Db;
import com.currencies.persistence.dao.CurrenciesDao;
import com.currencies.persistence.entities.Currency;

import java.util.List;
import java.util.Map;

import io.reactivex.Single;

import static androidx.room.Room.databaseBuilder;
import static io.reactivex.Single.fromCallable;
import static io.reactivex.schedulers.Schedulers.io;

public class CurrenciesRepository {

    public Single<RatesModel> getRates(String baseCurrencyCode) {
        return ApiHolder
                .getInstance()
                .getRevolutApi()
                .getRates(baseCurrencyCode.toUpperCase())
                .subscribeOn(io());
    }

    public Single<List<Currency>> updateCurrencies(Single<RatesModel> ratesSingle, String baseCurrencyCode) {
        CountriesApi countriesApi = ApiHolder.getInstance().getCountriesApi();
        CurrenciesDao currenciesDao = getCurrenciesDao();
        return ratesSingle.flatMap(rates -> {
            rates.getRates().put(baseCurrencyCode, 1d);
            for (Map.Entry<String, Double> rate : rates.getRates().entrySet()) {
                List<CountryModel> countries = countriesApi
                        .getCountriesByCurrency(rate.getKey())
                        .subscribeOn(io())
                        .blockingFirst();
                Currency currency;
                if (countries.size() == 0) {
                    continue;
                } else if (countries.size() == 1) {
                    CountryModel c = countries.get(0);
                    String name = "Unknown";
                    for (CurrencyModel cur : c.getCurrencies()) {
                        if (rate.getKey().equals(cur.getCode())) {
                            name = cur.getName();
                            break;
                        }
                    }
                    currency = new Currency(rate.getKey(), name, false, c.getFlag(), rate.getValue());
                } else {
                    currency = new Currency(rate.getKey(), "Multiple countries", true, null, rate.getValue());
                }
                currenciesDao.insert(currency);
            }
            return fromCallable(currenciesDao::getAll);
        });
    }

    public Single<List<Currency>> getCurrencies() {
        return fromCallable(getCurrenciesDao()::getAll);
    }

    private CurrenciesDao getCurrenciesDao() {
        return databaseBuilder(CurrenciesApplication.getInstance(), Db.class, "database-name")
                .build()
                .getCurrenciesDao();
    }
}