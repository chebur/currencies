package com.currencies.domain.interactors;

import com.currencies.domain.repository.CurrenciesRepository;
import com.currencies.persistence.entities.Currency;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public class CurrenciesInteractor {

    private final CurrenciesRepository repository;

    public CurrenciesInteractor() {
        this.repository = new CurrenciesRepository();
    }

    public Completable updateCurrencies(String baseCurrencyCode) {
        return Completable.fromSingle(repository.updateCurrencies(repository.getRates(baseCurrencyCode), baseCurrencyCode));
    }

    public Single<List<Currency>> getCurrencies() {
        return repository.getCurrencies();
    }
}
