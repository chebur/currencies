package com.currencies.domain.api;

import com.currencies.domain.model.CountryModel;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface CountriesApi {

    @GET("currency/{code}")
    Observable<List<CountryModel>> getCountriesByCurrency(@Path("code") String code);

}