package com.currencies.domain.api;

import com.currencies.domain.model.RatesModel;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RevolutApi {

    @GET("latest")
    Single<RatesModel> getRates(@Query("base") String base);

}
