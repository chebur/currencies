package com.currencies.domain.api;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiHolder {

    private static final String REVOLUT_ENDPOINT = "https://revolut.duckdns.org/";
    private static final String COUNTRY_INFO_ENDPOINT = "https://restcountries.eu/rest/v2/";

    private static volatile ApiHolder instance;

    private RevolutApi revolutApi;
    private CountriesApi countriesApi;

    public static ApiHolder getInstance() {
        ApiHolder localInstance = instance;
        if (localInstance == null) {
            synchronized (ApiHolder.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new ApiHolder();
                }
            }
        }
        return localInstance;
    }

    public RevolutApi getRevolutApi() {
        if (revolutApi == null) {
            revolutApi = (new Retrofit.Builder())
                    .baseUrl(REVOLUT_ENDPOINT)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()
                    .create(RevolutApi.class);
        }
        return revolutApi;
    }

    public CountriesApi getCountriesApi() {
        if (countriesApi == null) {
            countriesApi = (new Retrofit.Builder())
                    .baseUrl(COUNTRY_INFO_ENDPOINT)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()
                    .create(CountriesApi.class);
        }
        return countriesApi;
    }

}
