package com.currencies.presentation.presenters;

import com.currencies.domain.interactors.CurrenciesInteractor;
import com.currencies.presentation.ui.activities.SplashActivity;

import io.reactivex.disposables.CompositeDisposable;

import static com.currencies.Constants.CURRENCY_BASE;

public class SplashPresenter {

    private SplashActivity activity;
    private final CurrenciesInteractor interactor;
    private final CompositeDisposable disposable;

    public SplashPresenter() {
        this.interactor = new CurrenciesInteractor();
        disposable = new CompositeDisposable();
    }

    public void attachView(SplashActivity activity) {
        this.activity = activity;
        disposable.add(
                interactor
                        .updateCurrencies(CURRENCY_BASE)
                        .subscribe(this.activity::onComplete, this.activity::onError));
    }

    public void detachView() {
        disposable.dispose();
        activity = null;
    }

}