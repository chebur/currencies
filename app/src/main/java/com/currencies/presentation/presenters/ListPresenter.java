package com.currencies.presentation.presenters;

import com.currencies.domain.interactors.CurrenciesInteractor;
import com.currencies.presentation.ui.activities.ListActivity;

import io.reactivex.disposables.CompositeDisposable;

import static com.currencies.Constants.CURRENCY_BASE;
import static io.reactivex.android.schedulers.AndroidSchedulers.mainThread;
import static io.reactivex.schedulers.Schedulers.io;

public class ListPresenter {

    private ListActivity activity;
    private final CurrenciesInteractor interactor;
    private final CompositeDisposable disposable;

    public ListPresenter() {
        this.interactor = new CurrenciesInteractor();
        disposable = new CompositeDisposable();
    }

    public void attachView(ListActivity activity) {
        this.activity = activity;
        disposable.add(
                interactor.getCurrencies()
                        .subscribeOn(io())
                        .observeOn(mainThread())
                        .subscribe(
                                currencies -> activity.onListInit(currencies, CURRENCY_BASE),
                                activity::onError));
    }

    public void detachView() {
        disposable.dispose();
        activity = null;
    }
}
