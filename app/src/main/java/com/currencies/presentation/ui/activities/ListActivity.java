package com.currencies.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.currencies.R;
import com.currencies.persistence.entities.Currency;
import com.currencies.presentation.ListAdapter;
import com.currencies.presentation.presenters.ListPresenter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import timber.log.Timber;

import static android.widget.Toast.LENGTH_LONG;
import static android.widget.Toast.makeText;

public class ListActivity extends AppCompatActivity {

    @BindView(R.id.rv_list)
    RecyclerView rvList;

    private ListPresenter presenter;
    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Timber.v("ListActivity#onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);
        rvList.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onStart() {
        Timber.v("ListActivity#onStart");
        super.onStart();
        presenter = new ListPresenter();
        presenter.attachView(this);
    }

    @Override
    protected void onStop() {
        Timber.v("ListActivity#onStop");
        super.onStop();
        presenter.detachView();
        presenter = null;
    }

    @Override
    protected void onDestroy() {
        Timber.v("ListActivity#onDestroy");
        if (rvList != null && rvList.getAdapter() != null) {
            ((ListAdapter) rvList.getAdapter()).destroy();
            rvList.setAdapter(null);
        }
        if (unbinder != null) {
            unbinder.unbind();
            unbinder = null;
        }
        super.onDestroy();
    }

    public void onListInit(List<Currency> currencies, String currencyBase) {
        Timber.v("ListActivity#onListInit");
        rvList.setAdapter(new ListAdapter(currencies, currencyBase, this));
    }

    public void onError(Throwable throwable) {
        Timber.e(throwable);
        makeText(this, "Some error occurred", LENGTH_LONG).show();
    }

    public void scrollToTop() {
        rvList.scrollToPosition(0);
    }
}
