package com.currencies.presentation.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.currencies.R;
import com.currencies.presentation.presenters.SplashPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import timber.log.Timber;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class SplashActivity extends AppCompatActivity {

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.tv_wait)
    TextView tvWait;

    @BindView(R.id.btn_again)
    Button btnAgain;

    private SplashPresenter presenter;
    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Timber.v("SplashActivity#savedInstanceState");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        unbinder = ButterKnife.bind(this);
    }

    @Override
    protected void onStart() {
        Timber.v("SplashActivity#onStart");
        super.onStart();
        presenter = new SplashPresenter();
        presenter.attachView(this);
        progressBar.setVisibility(VISIBLE);
        tvWait.setVisibility(VISIBLE);
        btnAgain.setVisibility(GONE);
    }

    @Override
    protected void onStop() {
        Timber.v("SplashActivity#onStop");
        super.onStop();
        presenter.detachView();
        presenter = null;
    }

    @Override
    protected void onDestroy() {
        Timber.v("SplashActivity#onDestroy");
        if (unbinder != null) {
            unbinder.unbind();
            unbinder = null;
        }
        super.onDestroy();
    }

    public void onComplete() {
        Timber.v("SplashActivity#onComplete");
        startActivity(new Intent(this, ListActivity.class));
    }

    public void onError(Throwable throwable) {
        Timber.e(throwable);
        progressBar.setVisibility(View.INVISIBLE);
        tvWait.setVisibility(GONE);
        btnAgain.setVisibility(VISIBLE);
    }
}
