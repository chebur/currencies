package com.currencies.presentation;

import android.content.ContentResolver;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.request.RequestOptions;
import com.currencies.R;
import com.currencies.persistence.entities.Currency;
import com.currencies.presentation.ui.activities.ListActivity;
import com.currencies.presentation.utils.svg.GlideApp;
import com.currencies.presentation.utils.svg.SvgSoftwareLayerSetter;

import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

    private final List<Currency> values;
    private ListActivity activity;
    private final DecimalFormat format = new DecimalFormat("####.##");
    private final RateChangedWatcher rateChangedWatcher = new RateChangedWatcher();

    public ListAdapter(List<Currency> values, String currencyBase, ListActivity activity) {
        this.values = new LinkedList<>();
        for (Currency c : values) {
            if (c.getCode().equalsIgnoreCase(currencyBase)) {
                this.values.add(0, c);
            } else {
                this.values.add(c);
            }
        }
        this.activity = activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(
                LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_currency, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Currency currency = values.get(position);
        holder.tvCode.setText(currency.getCode());
        holder.tvName.setText(currency.getName());

        holder.etRate.removeTextChangedListener(rateChangedWatcher);
        holder.etRate.setText(format.format(currency.getRate()));
        holder.etRate.addTextChangedListener(rateChangedWatcher);

        holder.etRate.setOnFocusChangeListener(new RateSelectedListener(currency));
        String flag = currency.getFlag();
        if (flag == null) {
            flag = ContentResolver.SCHEME_ANDROID_RESOURCE + "://"
                    + holder.tvCode.getContext().getPackageName() + "/" + R.raw.multi;
        }
        holder.loadFlag(flag);
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public void destroy() {
        activity = null;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_flag)
        ImageView ivFlag;

        @BindView(R.id.tv_code)
        TextView tvCode;

        @BindView(R.id.tv_name)
        TextView tvName;

        @BindView(R.id.et_rate)
        EditText etRate;

        private final RequestBuilder<PictureDrawable> requestBuilder;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            requestBuilder = GlideApp.with(itemView.getContext())
                    .as(PictureDrawable.class)
                    .transition(withCrossFade())
                    .listener(new SvgSoftwareLayerSetter());
        }

        void loadFlag(String url) {
            requestBuilder
                    .load(Uri.parse(url))
                    .apply(RequestOptions.circleCropTransform())
                    .into(ivFlag);
        }
    }

    private class RateSelectedListener implements View.OnFocusChangeListener {
        private final Currency currency;

        RateSelectedListener(Currency currency) {
            this.currency = currency;
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            for (int i = 1; i < values.size(); i++) {
                Currency c = values.get(i);
                if (c.getCode().equals(currency.getCode())) {
                    values.remove(c);
                    values.add(0, c);
                    notifyItemMoved(i, 0);
                    activity.scrollToTop();
                    break;
                }
            }
        }
    }

    private class RateChangedWatcher implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // do not need
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // do not need
        }

        @Override
        public void afterTextChanged(Editable s) {
            double baseOld = values.get(0).getRate();
            double baseNew;
            try {
                baseNew = Double.parseDouble(s.toString());
            } catch (NumberFormatException e) {
                return;
            }
            for (int i = 1; i < values.size(); i++) {
                Currency c = values.get(i);
                c.setRate(c.getRate() * baseNew / baseOld);
            }
            values.get(0).setRate(baseNew);
            notifyItemRangeChanged(1, values.size() - 1);
        }
    }
}