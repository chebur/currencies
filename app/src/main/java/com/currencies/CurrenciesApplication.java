package com.currencies;

import android.app.Application;

import timber.log.Timber;

public class CurrenciesApplication extends Application {

    private static CurrenciesApplication instance = null;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Timber.plant(new Timber.DebugTree());
    }

    public static CurrenciesApplication getInstance() {
        return instance;
    }
}
