package com.currencies.persistence.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.currencies.persistence.entities.Currency;

import java.util.List;

@Dao
public interface CurrenciesDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Currency currency);

    @Query("SELECT * FROM currency")
    List<Currency> getAll();
}
