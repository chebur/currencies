package com.currencies.persistence.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

@Entity
public class Currency {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "code")
    private String code;

    @ColumnInfo(name = "name")
    private String name;

    @Nullable
    @ColumnInfo(name = "multiple")
    private Boolean multiple;

    @Nullable
    @ColumnInfo(name = "flag")
    private String flag;

    @ColumnInfo(name = "rate")
    private Double rate;

    public Currency(@NonNull String code, String name, @Nullable Boolean multiple, @Nullable String flag, Double rate) {
        this.code = code;
        this.name = name;
        this.multiple = multiple;
        this.flag = flag;
        this.rate = rate;
    }

    @NonNull
    public String getCode() {
        return code;
    }

    public void setCode(@NonNull String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Nullable
    public Boolean getMultiple() {
        return multiple;
    }

    public void setMultiple(@Nullable Boolean multiple) {
        this.multiple = multiple;
    }

    @Nullable
    public String getFlag() {
        return flag;
    }

    public void setFlag(@Nullable String flag) {
        this.flag = flag;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }
}