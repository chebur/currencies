package com.currencies.persistence;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.currencies.persistence.dao.CurrenciesDao;
import com.currencies.persistence.entities.Currency;

@Database(version = 1, entities = {Currency.class}, exportSchema = false)
abstract public class Db extends RoomDatabase {

    abstract public CurrenciesDao getCurrenciesDao();

}
